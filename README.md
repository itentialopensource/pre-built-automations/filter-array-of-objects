<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

<!-- Update the below line with your artifact name -->
# Filter Array of Objects

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
This Pre-Built Transformation allows you to filter any array of objects by providing a key and a value to match on, and the option to discard the matching items from the array.

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1.x`


## How to Install

To install the artifact:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the artifact. 
* The artifact can be installed from within App-Admin_Essential. Simply search for the name of your desired artifact and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

To use this JST, provide an array of objects as an input. Also provide a key and a value to find. Lastly, choose whether you want to discard or keep changes.

For example, this will return all the object matching key "state" and value "GA":
```
{
  "filterableArray": [
    {"city":"Atlanta", "state":"GA"},
    {"city":"Sandy Springs", "state":"GA"},
    {"city":"Dallas", "state":"TX"},
    {"city":"Orlando", "state":"FL"}
  ],
  "keyToFilterOn": "state",
  "valueToFind": "GA",
  "discardMatching": false
}
```

Output: 

```
{
  "filteredArray": [
    {
      "city": "Atlanta",
      "state": "GA"
    },
    {
      "city": "Sandy Springs",
      "state": "GA"
    }
  ],
  "firstFilteredElement": {
    "city": "Atlanta",
    "state": "GA"
  }
}
```

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
