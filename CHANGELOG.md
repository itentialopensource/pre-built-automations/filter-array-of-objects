
## 0.1.10 [01-30-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!9

---

## 0.1.9 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!8

---

## 0.1.8 [06-08-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!7

---

## 0.1.7 [12-03-2021]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!6

---

## 0.1.6 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!5

---

## 0.1.5 [05-12-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!4

---

## 0.1.4 [12-23-2020]

* [master] Replaced equals with deepEquals to handle nested objects

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!3

---

## 0.1.3 [11-30-2020]

* [master] Replaced equals with deepEquals to handle nested objects

See merge request itentialopensource/pre-built-automations/filter-array-of-objects!3

---

## 0.1.2 [07-14-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/filter-array-of-objects!2

---

## 0.1.1 [07-14-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/filter-array-of-objects!2

---

## 0.1.0 [07-14-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/filter-array-of-objects!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
